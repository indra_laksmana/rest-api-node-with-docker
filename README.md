# Node API for Telkom Blanja.com Test Website

## Getting Started
This app uses **node.js**, **docker-ce**, **docker-compose** and **treafik network** as **proxy**. So, please install all requirement on your **linux** system before build this app.

## How to build app?
Just run below command to build app.

### Build with development environment 
```
docker-compose -f docker-compose.dev.yml up --build
```

### Build with production environment
```
docker-compose up --build
```


