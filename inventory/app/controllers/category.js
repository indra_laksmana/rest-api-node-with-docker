const categoryModel = require('../models/category');
const productModel = require('../models/product');
const productCategoryModel = require('../models/productCategory');
const productImageModel = require('../models/productImage');
const storeModel = require('../models/store');

module.exports = {
	getAllCategory: function (req, res, next) {
		categoryModel.getAllCategory(function (err, category) {
			if (err){
				res.send(500).json({ error: err });
				next(err);
			}else{
				res.status(200).json({ data: { category } });
			}
		});
	},
	getCategoryById: function (req, res, next) {
		categoryModel.getCategoryById(req.params.id,function (err, categ) {
			if (err) {
				res.send(500).json({ error: err });
				next(err);
			} else {
				productCategoryModel.getProductByCategoryId(req.params.id, function (err, productIds) {
					if (err) {
						res.send(500).json({ error: err });
						next(err);
					} else {
						var ids = [];
						var prodIds = {};
						for(var i=0;i < productIds.length;i++){
							ids.push(productIds[i].product_id); 
						}
						prodIds = ids;
						productModel.getProductByIds(prodIds, function (err, products) {
							if (err) {
								res.send(500).json({ error: err });
								next(err);
							} else {
								var i = products.length;
								var prd = [];
								var ct = [];
								var cat = categ[0];
								var product = JSON.parse(JSON.stringify(products));
								k = 0;
								j = products.length;
								l = products.length;
								while(i > 0 ){
									prod = {};
									productImageModel.getImagesByProductId(product[k].id, function (err, images) {
										if (err) {
											res.send(500).json({ error: err });
											next(err);
										} else {
											productCategoryModel.getCategoryByProductId(product[j].id, function (err, getCategory) {
												if (err) {
													res.send(500).json({ error: err });
													next(err);
												} else {
													categoryModel.getCategoryById(getCategory[0].category_id, function (err, cate) {
														if (err) {
															res.send(500).json({ error: err });
															next(err);
														} else {
															//console.log(i++,products.length-1);
															//console.log(j);
															storeModel.getStoreById(product[l].store_id, function (err, store) {
																if (err) {
																	res.send(500).json({ error: err });
																	next(err);
																} else {
																	prd.push({
																		'id': product[i].id,
																		'name': product[i].name,
																		'description': product[i].description,
																		'stock': product[i].stock,
																		'harga': product[i].harga,
																		'store_id': product[i].store_id,
																		'created_at': product[i].created_at,
																		'updated_at': product[i].updated_at,
																		'image': images,
																		'category': cate,
																		'store': store
																	});

																	if (i++ === (product.length - 1)) {
																		ct.push({
																			'id': cat.id,
																			'name': cat.name,
																			'created_at': cat.created_at,
																			'updated_at': cat.updated_at,
																			'product': prd
																		});
																		res.status(200).json({ data: { category: ct[0] } });
																	}

																}
															});
															l++;
														} 
													});
												} 
											});
											j++;
										}
									});
									k++;
									j--;
									i--;
									l--;
								}
							}
						})
					}
				});
			}
		});
	}
}