const productModel = require('../models/product');
const productImageModel = require('../models/productImage');
const productCategoryModel = require('../models/productCategory');
const categoryModel = require('../models/category');
const storeModel = require('../models/store');

module.exports = {
	getAllProduct: function name(req, res, next) {
		productModel.getAllProduct(function (err, prods) {
			if (err) {
				res.send(500).json({ error: err });
				next(err);
			} else {
				var i = prods.length;
				var prd = [];
				prods.forEach(prod => {
					productImageModel.getImagesByProductId(prod.id, function (err, images) {
						if (err) {
							res.send(500).json({ error: err });
							next(err);
						} else {
							productCategoryModel.getCategoryByProductId(prod.id, function (err, getCategory) {
								if (err) {
									res.send(500).json({ error: err });
									next(err);
								} else {
									categoryModel.getCategoryById(getCategory[0].category_id, function (err, cate) {
										if (err) {
											res.send(500).json({ error: err });
											next(err);
										} else {
											//console.log(i++,products.length-1);
											//console.log(j);
											storeModel.getStoreById(prod.store_id, function (err, store) {
												if (err) {
													res.send(500).json({ error: err });
													next(err);
												} else {
													prd.push({
														'id': prod.id,
														'name': prod.name,
														'description': prod.description,
														'stock': prod.stock,
														'harga': prod.harga,
														'store_id': prod.store_id,
														'created_at': prod.created_at,
														'updated_at': prod.updated_at,
														'image': images,
														'category': cate,
														'store': store
													});

													if (i++ === (prods.length - 1)) {
														res.status(200).json({ data: { product: prd } });
													}

												}
											});
										}
									});
								}
							});
						}
					});
					i--;
				});
			}
		});
	},
	getProductById: function (req, res, next) {
		var dt = [];
		productModel.getProductById(req.params.id, function (err, prod) {
			if (err) {
				res.send(500).json({ error: err });
				next(err);
			} else {
				productImageModel.getImagesByProductId(req.params.id, function (err, images) {
					if (err) {
						res.send(500).json({ error: err });
						next(err);
					} else {
						productCategoryModel.getCategoryByProductId(req.params.id, function (err, getCategory) {
							if (err) {
								res.send(500).json({ error: err });
								next(err);
							} else {
								categoryModel.getCategoryById(getCategory[0].category_id, function (err, category) {
									if (err) {
										res.send(500).json({ error: err });
										next(err);
									} else {
										storeModel.getStoreById(prod[0].store_id, function (err, store) {
											if (err) {
												res.send(500).json({ error: err });
												next(err);
											} else {
												productCategoryModel.getRelatedProduct(getCategory[0].category_id, function (err, productIds) {
													if (err) {
														res.send(500).json({ error: err });
														next(err);
													} else {
														var ids = [];
														var prodIds = {};
														for (var i = 0; i < productIds.length; i++) {
															ids.push(productIds[i].product_id);
														}
														prodIds = ids;
														productModel.getProductByIds(prodIds, function (err, products) {
															if (err) {
																res.send(500).json({ error: err });
																next(err);
															} else {
																var i = products.length;
																var prd = [];
																var product = JSON.parse(JSON.stringify(products));
																k = 0;
																j = products.length;
																l = products.length;
																while (i > 0) {
																	productImageModel.getImagesByProductId(product[k].id, function (err, images) {
																		if (err) {
																			res.send(500).json({ error: err });
																			next(err);
																		} else {
																			productCategoryModel.getCategoryByProductId(product[j].id, function (err, getCateg) {
																				if (err) {
																					res.send(500).json({ error: err });
																					next(err);
																				} else {
																					categoryModel.getCategoryById(getCateg[0].category_id, function (err, cate) {
																						if (err) {
																							res.send(500).json({ error: err });
																							next(err);
																						} else {
																							//console.log(i++,products.length-1);
																							//console.log(j);
																							storeModel.getStoreById(product[l].store_id, function (err, stores) {
																								if (err) {
																									res.send(500).json({ error: err });
																									next(err);
																								} else {
																									prd.push({
																										'id': product[i].id,
																										'name': product[i].name,
																										'description': product[i].description,
																										'stock': product[i].stock,
																										'harga': product[i].harga,
																										'store_id': product[i].store_id,
																										'created_at': product[i].created_at,
																										'updated_at': product[i].updated_at,
																										'image': images,
																										'category': cate,
																										'store': stores[0]
																									});

																									if (i++ === (product.length - 1)) {
																										dt.push({
																											product: {
																												id: prod[0].id,
																												name: prod[0].name,
																												description: prod[0].description,
																												harga: prod[0].harga,
																												stock: prod[0].stock,
																												store_id: prod[0].store_id,
																												created_at: prod[0].created_at,
																												updated_at: prod[0].updated_at,
																												image: images,
																												category,
																												store: store[0],
																												related_product: prd
																											}
																										});
																										res.status(200).json({ data: dt[0] });
																									}

																								}
																							});
																							l++;
																						}
																					});
																				}
																			});
																			j++;
																		}
																	});
																	k++;
																	j--;
																	i--;
																	l--;
																}
															}
														})
													}
												});
											}
										});
									}
								});
							}
						});
					}
				})
			}
		});
	}
}