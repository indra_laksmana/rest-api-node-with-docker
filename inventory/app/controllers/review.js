const reviewProductModel = require('../models/reviewProduct');
const userModel = require('../models/user');

module.exports = {
	getReviewProduct: function (req, res, next) {
		reviewProductModel.getReviewByProductId(req.params.id,function (err, review) {
			if (err) {
				res.send(500).json({ error: err });
				next(err);
			} else {
				var i = review.length;
				var j = 0;
				var rev = [];
				while(i > 0){
					userModel.getUserById(review[j].user_id, function (err, user) {
						if (err) {
							res.send(500).json({ error: err });
							next(err);
						} else {
							//console.log(i, review.length - 1);
							rev.push({
								id: review[i].id,
								product_id: review[i].product_id,
								user_id: review[i].user_id,
								review: review[i].review,
								rating: review[i].rating,
								created_at: { date: review[i].created_at },
								updated_at: { date: review[i].updated_at },
								user
							});

							if (i++ === review.length - 1){
								res.status(200).json({ data: { productReview: rev } });
							}
						}
					});
					i--;
					j++;
				}
			}
		});
	}
}