const wishlistModel = require('../models/wishlist');
const productModel = require('../models/product');
const productImageModel = require('../models/productImage');
const productCategoryModel = require('../models/productCategory');
const categoryModel = require('../models/category');
const storeModel = require('../models/store');
const userModel = require('../models/user');

module.exports = {
	createWishlist: function (req, res, next) {
		var data = [];
		data.push({
			user_id: req.body.user_id,
			product_id: req.body.product_id
		});
		wishlistModel.addItemToWishlist(data[0], function (err, wishlist) {
			if (err) {
				res.send(500).json({ error: err });
				next(err);
			} else {
				res.status(200).json({ success: 'Wishlist created' });
			}
		});
	},
	getWishlist: function (req, res, next) {
		userModel.getUserById(req.params.id, function name(err, user) {
			if (err) {
				res.send(500).json({ error: err });
				next(err);
			} else {
				wishlistModel.getWishlistByUserId(req.params.id, function (err, wishlist) {
					if (err) {
						res.send(500).json({ error: err });
						next(err);
					} else {
						var dt = [];
						var ids = [];
						wishlist.forEach((wish) => {
							ids.push(wish.product_id);
						});
						productModel.getProductByIds(ids, function (err, product) {
							if (err) {
								res.send(500).json({ error: err });
								next(err);
							} else {
								var i = product.length;
								var j = 0;
								while(i > 0){
									productImageModel.getImagesByProductId(product[j].id, function (err, images) {
										if (err) {
											res.send(500).json({ error: err });
											next(err);
										} else {
											productCategoryModel.getCategoryByProductId(product[i].id, function (err, getCategory) {
												if (err) {
													res.send(500).json({ error: err });
													next(err);
												} else {
													categoryModel.getCategoryById(getCategory[0].category_id, function (err, category) {
														if (err) {
															res.send(500).json({ error: err });
															next(err);
														} else {
															//console.log(i++, product.length - 1);
															storeModel.getStoreById(product[i].store_id, function (err, store) {
																if (err) {
																	res.send(500).json({ error: err });
																	next(err);
																} else {
																	// console.log(i, wishlist.length - 1);
																	dt.push({
																		id: wishlist[i].id,
																		user_id: wishlist[i].user_id,
																		product_id: wishlist[i].product_id,
																		created_at: wishlist[i].created_at,
																		product: {
																			id: product[i].id,
																			name: product[i].name,
																			description: product[i].description,
																			harga: product[i].harga,
																			stock: product[i].stock,
																			store_id: product[i].store_id,
																			created_at: product[i].created_at,
																			updated_at: product[i].updated_at,
																			image: images,
																			category,
																			store: store[0]
																		}
																	});
																	if (i === (product.length - 1)) {
																		res.status(200).json({ data: { wishlist: dt } });
																	}
																}
																i++;
															});
														}
													});
												}
											});
										}
									});
									i--;
									j++;
								}
							}
						});
					}
				});
			}
		});
	}
}