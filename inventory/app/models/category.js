const db = require('../../config/database');

//Category object constructor
const Category = function (category) {
	this.name = category.name;
	this.created_at = new Date();
};
Category.getCategoryById = function (categoryId, result) {
	db.query("SELECT * FROM category WHERE id = ? ", categoryId, function (err, res) 
	{
		if (err) {
			console.log("error: ", err);
			result(err, null);
		}
		else {
			result(null, res);
		}
	});
};
Category.getAllCategory = function (result) {
	db.query("SELECT * FROM category", function (err, res) 
	{
		if (err) {
			console.log("error: ", err);
			result(null, err);
		}
		else {
			result(null, res);
		}
	});
};

module.exports = Category;