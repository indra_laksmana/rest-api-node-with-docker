const db = require('../../config/database');

//ProductCategory object constructor
const ProductCategory = function (productCategory) {
	this.product_id = productCategory.product_id;
	this.category_id = productCategory.category_id;
	this.created_at = new Date();
};
ProductCategory.getProductByCategoryId = function (categoryId, result) {
	db.query("SELECT * FROM product_category WHERE category_id = ? ", categoryId, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		}
		else {
			result(null, res);
		}
	});
};
ProductCategory.getCategoryByProductId = function (productId, result) {
	db.query("SELECT category_id FROM product_category WHERE product_id = ? ", productId, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		}
		else {
			result(null, res);
		}
	});
};
ProductCategory.getRelatedProduct = function (categoryId, result) {
	db.query("SELECT * FROM product_category WHERE category_id = ? LIMIT 5", categoryId, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		}
		else {
			result(null, res);
		}
	});
};

module.exports = ProductCategory;