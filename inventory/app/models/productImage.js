const db = require('../../config/database');

//ProductImage object constructor
const ProductImage = function (productImage) {
	this.product_id = productImage.product_id;
	this.image = productImage.image;
	this.created_at = new Date();
};
ProductImage.getImagesByProductId = function (productId, result) {
	db.query("SELECT * FROM product_image WHERE product_id = ?", productId, function (err, res) {
		if (err) {
			console.log("error: ", err);
		} else {
			return result(null, res);
		}
	});
};

module.exports = ProductImage;