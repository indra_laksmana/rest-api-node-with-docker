const db = require('../../config/database');

//ReviewProduct object constructor
const ReviewProduct = function (reviewProduct) {
	this.id = reviewProduct.id;
	this.product_id = reviewProduct.product_id;
	this.user_id = reviewProduct.user_id;
	this.review = reviewProduct.review;
	this.rating = reviewProduct.rating;
	this.created_at = new Date();
};
ReviewProduct.getReviewByProductId = function (productId, result) {
	db.query("SELECT * FROM product_review WHERE product_id = ?", productId, function (err, res) {
		if (err) {
			console.log("error: ", err);
		} else {
			return result(null, res);
		}
	});
};

module.exports = ReviewProduct;