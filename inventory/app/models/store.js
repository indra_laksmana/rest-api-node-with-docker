const db = require('../../config/database');

//Store object constructor
const Store = function (store) {
	this.user_id = store.user_id;
	this.name = store.name;
	this.description = store.description;
	this.image = store.image;
	this.created_at = new Date();
};
Store.getStoreById = function (storeId, result) {
	db.query("SELECT * FROM store WHERE id = ? ", storeId, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		}
		else {
			result(null, res);
		}
	});
};

module.exports = Store;