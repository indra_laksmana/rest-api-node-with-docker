const db = require('../../config/database');

//User object constructor
const User = function (user) {
	this.id = user.id;
	this.email = user.email;
	this.password = user.password;
	this.username = user.username;
	this.name = user.name;
	this.phone = user.phone;
	this.address = user.address;
	this.created_at = new Date();
};
User.getUserById = function (userId, result) {
	db.query("SELECT * FROM users WHERE id = ? ", userId, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		}
		else {
			result(null, res);
		}
	});
};

module.exports = User;