const db = require('../../config/database');

//Wishlist object constructor
const Wishlist = function (wishlist) {
	this.id = wishlist.id;
	this.user_id = wishlist.user_id;
	this.product_id = wishlist.product_id;
	this.created_at = new Date();
};
Wishlist.addItemToWishlist = function (data, result) {
	db.query("INSERT INTO wishlist SET user_id = " + data.user_id + ", product_id = " + data.product_id + ", created_at = '" + new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '') + "'", function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		}
		else {
			result(null, res);
		}
	});
};
Wishlist.getWishlistByUserId = function (userId, result) {
	db.query("SELECT * FROM wishlist WHERE user_id = ? ", userId, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		}
		else {
			result(null, res);
		}
	});
};

module.exports = Wishlist;