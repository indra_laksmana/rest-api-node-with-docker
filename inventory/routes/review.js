const express = require('express');
const router = express.Router();

const reviewController = require('../app/controllers/review');

router.get('/:id', reviewController.getReviewProduct);

module.exports = router;