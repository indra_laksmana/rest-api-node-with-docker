const express = require('express');
const router = express.Router();
const multer = require('multer');
const formData = multer();

const wishlistController = require('../app/controllers/wishlist');

router.post('/', formData.fields([]), wishlistController.createWishlist);
router.get('/:id', formData.fields([]), wishlistController.getWishlist);

module.exports = router;