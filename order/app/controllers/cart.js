const cartModel = require('../models/cart');
const userModel = require('../models/user');
const productModel = require('../models/product');
const invoiceModel = require('../models/invoice');

module.exports = {
	createCart: function (req, res, next) {
		var items = [];
		var productId = req.body.product_id;
		var qty = req.body.qty;
		var i = 0;
		while(i < productId.length){
			items.push({
				product_id: productId[i],
				qty: qty[i],
				user_id: req.body.user_id
			});
			i++;
		}
		var j = items.length;
		items.forEach((item) => {
			cartModel.putItemsIntoCart(item, function (err) {
				if (err) {
					res.send(500).json({ error: err });
					next(err);
				} else {
					if (j++ === items.length - 1){
						res.status(200).json({ success: 'Cart created' });
					}
				}
			});
			j--;
		});
	},
	getCheckout: function name(req, res, next) {
		cartModel.getItemFromCart(req.params.id, function (err, cart) {
			if (err) {
				res.send(500).json({ error: err });
				next(err);
			} else {
				var dt = [];
				var i = cart.length;
				var crt = [];
				cart.forEach((item) => {
					productModel.getProductById(item.product_id, function (err, prod) {
						if (err) {
							res.send(500).json({ error: err });
							next(err);
						} else {
							//console.log(i++, cart.length - 1);
							crt.push({
								id: item.id,
								created_at: item.created_at,
								price: item.price,
								status: item.status,
								product_id: item.product_id,
								user_id: item.user_id,
								qty: item.qty,
								invoice_id: item.invoice_id,
								product: [{
									id: prod[0].id,
									name: prod[0].name,
									description: prod[0].description,
									stock: prod[0].stock,
									harga: prod[0].harga,
									store_id: prod[0].store_id,
									created_at: {
										date: prod[0].created_at
									},
									updated_at: { 
										date: prod[0].updated_at
									}
								}] 
							});
							if (i++ === cart.length - 1){
								userModel.getUserById(req.params.id, function (err, user) {
									if (err) {
										res.send(500).json({ error: err });
										next(err);
									} else {
										dt.push({
											cart: crt,
											user
										});
										res.status(200).json({ data: dt[0] });
									}
								});
							}
						}	
					});
					i--;
				});
			}
		});		
	},
	sendCommit: function name(req, res, next) {
		cartModel.getTotalFromCart(req.params.id, function (err, call) {
			if (err) {
				res.send(500).json({ error: err });
				next(err);
			} else {
				//console.log(call[0].total);
				var data = [];
				if (req.body.promo_code){
					data.push({
						total: call[0].total,
						user_id: req.params.id,
						address: req.body.address,
						method: req.body.method,
						promo_code: req.body.promo_code
					});
				}else{
					data.push({
						total: call[0].total,
						user_id: req.params.id,
						address: req.body.address,
						method: req.body.method
					});
				}
				invoiceModel.createInvoice(data[0], function (err, result) {
					if (err) {
						res.send(500).json({ error: err });
						next(err);
					} else {
						var dt = [];
						dt.push({
							user_id: req.params.id,
							invoice_id: result.insertId
						});
						cartModel.changeStatusItemInCart(dt[0], function (err, cart) {
							if (err) {
								res.send(500).json({ error: err });
								next(err);
							} else {
								var rsl = [];
								invoiceModel.getInvoiceById(result.insertId, function (err, invoice) {
									if (err) {
										res.send(500).json({ error: err });
										next(err);
									} else {
										cartModel.getItemByInvoiceId(result.insertId, function (err, items) {
											if (err) {
												res.send(500).json({ error: err });
												next(err);
											} else {
												rsl.push({
													id: invoice[0].id,
													total: invoice[0].total,
													user_id: invoice[0].user_id,
													address: invoice[0].address,
													status: invoice[0].status,
													method: invoice[0].method,
													created_at: invoice[0].created_at,
													cart: items
												});
												res.status(200).json({ data: { invoice: rsl[0] } });
											}
										});	
									}
								});
							}
						});
					}
				});
			} 
		});
	}
}