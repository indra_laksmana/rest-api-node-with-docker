const promoModel = require('../models/promo');

module.exports = {
	getPromo: function (req, res, next) {
		promoModel.getPromoByCode(req.params.code, function (err, promo) {
			if (err) {
				res.send(500).json({ error: err });
				next(err);
			} else {
				var prm = [];
				prm.push({
					code: promo[0].code,
					value: promo[0].value,
					begin_date: promo[0].begin_date,
					end_date: promo[0].end_date,
					created_at: {
						date: promo[0].created_at
					},
					updated_at: {
						date: promo[0].updated_at
					}
				})
				res.status(200).json({ data: { promo: prm[0] } });
			}
		});
	}
}