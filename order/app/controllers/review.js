const reviewProductModel = require('../models/reviewProduct');

module.exports = {
	sendReviewProduct: function (req, res, next) {
		var data = [];
		data.push({
			user_id: req.body.user_id,
			review: req.body.review,
			rating: req.body.rating,
			product_id: req.body.product_id
		});
		reviewProductModel.createReview(data[0],function (err, review) {
			if (err) {
				res.send(500).json({ error: err });
				next(err);
			} else {
				res.status(200).json({ success: 'Product review created' });
			}
		});
	}
}