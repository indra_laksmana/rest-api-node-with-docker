const db = require('../../config/database');

//cart object constructor
const Cart = function (cart) {
	this.id = cart.id;
	this.product_id = cart.product_id;
	this.user_id = cart.user_id;
	this.review = cart.review;
	this.rating = cart.rating;
	this.created_at = new Date();
};
Cart.putItemsIntoCart = function (item, result) {
	db.query("DELETE FROM cart WHERE user_id = ? AND status='incomplete'", item.user_id, function (err, del) {
		if (err) {
			console.log("error: ", err);
		} else {
			db.query("SELECT * FROM product WHERE id = ?", item.product_id, function (err, product) {
				if (err) {
					console.log("error: ", err);
				} else {
					db.query("INSERT INTO cart SET product_id = " + item.product_id + ", user_id = " + item.user_id + ", qty = " + item.qty + ", price = " + product[0].harga + ", status = 'incomplete', created_at = '" + new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '') + "'", function (err, res) {
						if (err) {
							console.log("error: ", err);
						} else {
							return result(null, res);
						}
					});
				}
			});
		} 
	});
};
Cart.getItemFromCart = function (userId, result) {
	db.query("SELECT * FROM cart WHERE user_id = ? AND status = 'incomplete'", userId, function (err, res) {
		if (err) {
			console.log("error: ", err);
		} else {
			return result(null, res);
		}
	});
};
Cart.changeStatusItemInCart = function (data, result) {
	db.query("UPDATE cart SET status = 'pending', invoice_id = " + data.invoice_id + " WHERE user_id = " + data.user_id + " AND status = 'incomplete'", function (err, res) {
		if (err) {
			console.log("error: ", err);
		} else {
			return result(null, res);
		}
	});
};
Cart.getTotalFromCart = function (userId, result) {
	db.query("SELECT SUM(price*qty) as total FROM cart WHERE user_id = ? AND status = 'incomplete'", userId, function (err, res) {
		if (err) {
			console.log("error: ", err);
		} else {
			return result(null, res);
		}
	});
};
Cart.getItemByInvoiceId = function (invoiceId, result) {
	db.query("SELECT * FROM cart WHERE invoice_id = ? AND status = 'pending'", invoiceId, function (err, res) {
		if (err) {
			console.log("error: ", err);
		} else {
			return result(null, res);
		}
	});
};

module.exports = Cart;