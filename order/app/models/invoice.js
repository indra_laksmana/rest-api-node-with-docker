const db = require('../../config/database');

//Invoice object constructor
const Invoice = function (invoice) {
	this.id = invoice.id;
	this.total = invoice.total;
	this.user_id = invoice.user_id;
	this.address = invoice.address;
	this.status = invoice.status;
	this.method = invoice.method;
	this.created_at = new Date();
	this.promo_code = invoice.promo_code;
};
Invoice.createInvoice = function (data, result) {
	var qry = "INSERT INTO invoice SET total = " + data.total + ", user_id = " + data.user_id + ", address = '" + data.address + "', status = 'waiting payment', method = '" + data.method + "', created_at = '" + new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '') + "'";
	if(data.promo_code){
		qry += ", promo_code = '" + data.promo_code + "'";
	}
	db.query(qry, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		}
		else {
			result(null, res);
		}
	});
};
Invoice.getInvoiceById = function (invoiceId, result) {
	db.query("SELECT * FROM invoice WHERE id = ? AND status = 'waiting payment'", invoiceId, function (err, res) {
		if (err) {
			console.log("error: ", err);
		} else {
			return result(null, res);
		}
	});
};

module.exports = Invoice;