const db = require('../../config/database');

//Product object constructor
const Product = function (product) {
	this.name = product.name;
	this.description = product.description;
	this.harga = product.harga;
	this.stock = product.stock;
	this.store_id = product.store_id;
	this.created_at = new Date();
};
Product.getProductById = function (productId, result) {
	db.query("SELECT * FROM product WHERE id = ? ", productId, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		}
		else {
			result(null, res);
		}
	});
};
Product.getProductByIds = function (productIds, result) {
	db.query("SELECT * FROM product WHERE id IN ("+productIds+")", function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		}
		else {
			result(null, res);
		}
	});
};
Product.getAllProduct = function (result) {
	db.query("SELECT * FROM product LIMIT 20", function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(null, err);
		}
		else {
			result(null, res);
		}
	});
};

module.exports = Product;