const db = require('../../config/database');

//Promo object constructor
const Promo = function (promo) {
	this.code = promo.code;
	this.value = promo.value;
	this.begin_date = promo.begin_date;
	this.end_date = promo.end_date;
	this.created_at = new Date();
};
Promo.getPromoByCode = function (promoCode, result) {
	db.query("SELECT * FROM promo WHERE code = ? ", promoCode, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		}
		else {
			result(null, res);
		}
	});
};

module.exports = Promo;