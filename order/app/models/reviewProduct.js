const db = require('../../config/database');

//ReviewProduct object constructor
const ReviewProduct = function (reviewProduct) {
	this.id = reviewProduct.id;
	this.product_id = reviewProduct.product_id;
	this.user_id = reviewProduct.user_id;
	this.review = reviewProduct.review;
	this.rating = reviewProduct.rating;
	this.created_at = new Date();
};
ReviewProduct.createReview = function (data, result) {
	db.query("INSERT INTO product_review SET user_id = " + data.user_id + ", review = '" + data.review + "', rating = " + data.rating + ", product_id = " + data.product_id + ", created_at = '" + new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '') + "'", data, function (err, res) {
		if (err) {
			console.log("error: ", err);
		} else {
			return result(null, res);
		}
	});
};

module.exports = ReviewProduct;