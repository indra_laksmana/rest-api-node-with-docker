const mysql = require('mysql');

//local mysql db connection
const connection = mysql.createConnection({
	host: 'rdbms',
	user: 'dev',
	password: 'secret',
	database: 'dev'
});

connection.connect(function (err) {
	if (err)
		console.log('Could not connect to MySQL. Exiting now...', err);
	else
		console.log('Successfully connected to MySQL');
});

module.exports = connection;