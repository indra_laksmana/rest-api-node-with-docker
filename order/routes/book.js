const express = require('express');
const router = express.Router();
const multer = require('multer');
const formData = multer();

const cartController = require('../app/controllers/cart');

router.get('/checkout/:id', formData.fields([]), cartController.getCheckout);
router.post('/commit/:id', formData.fields([]), cartController.sendCommit);

module.exports = router;