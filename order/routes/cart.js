const express = require('express');
const router = express.Router();
const multer = require('multer');
const formData = multer();

const cartController = require('../app/controllers/cart');

router.post('/bulk', formData.fields([]), cartController.createCart);

module.exports = router;