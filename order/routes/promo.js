const express = require('express');
const router = express.Router();
const multer = require('multer');
const formData = multer();

const promoController = require('../app/controllers/promo');

router.get('/:code', formData.fields([]), promoController.getPromo);

module.exports = router;