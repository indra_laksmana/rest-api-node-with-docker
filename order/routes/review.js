const express = require('express');
const router = express.Router();
const multer = require('multer');
const formData = multer();

const reviewController = require('../app/controllers/review');

router.post('/', formData.fields([]), reviewController.sendReviewProduct);

module.exports = router;