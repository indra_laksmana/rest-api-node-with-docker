const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors')
const app = express();

const cart = require('./routes/cart');
const book = require('./routes/book');
const promo = require('./routes/promo');
const review = require('./routes/review');

app.use(cors());

// connection to mysql

app.use(logger('dev'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/', function (req, res) {
	res.json({ "ORDER-NODE-API": "Build REST API with node.js" });
});

// public route
app.use('/api/v1/cart', cart);
app.use('/api/v1/book', book);
app.use('/api/v1/promo', promo);
app.use('/api/v1/review', review);

// express doesn't consider not found 404 as an error so we need to handle 404 explicitly
// handle 404 error
app.use(function (req, res, next) {
	let err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// handle errors
app.use(function (err, req, res, next) {
	console.log(err);

	if (err.status === 404)
		res.status(404).json({ message: "Not found" });
	else
		res.status(500).json({ message: "Something looks wrong!!!" });
});

app.listen(4002, function () { console.log('Node server listening on port 4002'); });