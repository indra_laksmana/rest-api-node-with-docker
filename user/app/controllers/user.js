const userModel = require('../models/user');
const argon2i = require('argon2-ffi').argon2i;
const crypto = require('crypto');
const Promise = require('bluebird');
const randomBytes = Promise.promisify(crypto.randomBytes);
const jwt = require('jsonwebtoken');
const config = require('../../config/app');
const redis = require('redis');
const client = redis.createClient(process.env.REDIS_URL);

module.exports = {
	register: function (req, res, next) {
		var password = new Buffer(req.body.password);
		var options = { timeCost: 2, memoryCost: 1 << 10, parallelism: 2, hashLength: 16 };

		userModel.findOne({ email: req.body.email }, function (err, user) {
			if (err) {
				res.status(500).json({ error: err });
				next(err);
			} else {
				if(user){
					res.status(200).json({ error: 'Email has already been taken' });
				}else{
					randomBytes(32).then(salt => argon2i.hash(password, salt, options))
						.then((encoded) => {
							userModel.findOne({}).sort({ _id: -1 }).exec(function (err, lastUser) {
								userModel.create({
									_id: parseInt(lastUser._id, 10) + 1,
									name: req.body.name,
									username: req.body.username,
									email: req.body.email,
									password: encoded
								}, function (err, userInfo) {
									if (err)
										next(err);
									else
										var token = jwt.sign({ id: userInfo._id }, config.secret, { expiresIn: '1d' });
									return res.status(200).json({
										data: {
											user: {
												email: userInfo.email,
												name: userInfo.name,
												username: userInfo.username,
												_id: userInfo._id
											},
											token: token
										}
									});
								});
							});
						});
				}
			} 
		});
	},
	login: function (req, res, next) {
		userModel.findOne({ email: req.body.email }, function (err, userInfo) {
			if (err) {
				res.status(500).json({ error: err });
				next(err);
			} else {
				let pass = new Buffer(req.body.password);
				argon2i.verify(userInfo.password, pass)
				.then((correct) => {
					if(correct){
						const token = jwt.sign({ id: userInfo._id }, config.secret, { expiresIn: '1d' });
						return res.status(200).json({ data: { token: token } });
					}else{
						return res.sendStatus(404).json({ status: "error", message: "Invalid email/password!!!" });	
					}
				});
			}
		});
	},
	me: function (req, res) {
		// check header or url parameters or post parameters for token
		let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase
		
		if (token.startsWith('Bearer ')) {
			// Remove Bearer from string
			token = token.slice(7, token.length);
		}

		jwt.verify(token, config.secret, (err, decoded) => {
			if (err) {
				return res.json({
					success: false,
					message: 'Token is not valid'
				});
			} else {
				client.get(token, function (err, reply) {
					if (reply == 'invalidated'){
						return res.status(404).json({ error: 'Token was invalidated' });
					}else{
						let userId = decoded.id;
						// Fetch the user by id 
						userModel.findOne({ _id: userId }).then(function (user) {
							// Do something with the user
							return res.status(200).json({ id: user._id, email: user.email, username: user.username, name: user.name, phone: user.phone, address: user.address });
						});
					}
				});
			}
		});

	},
	invalidate: function (req, res) {

		// check header or url parameters or post parameters for token
		let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase

		if (token.startsWith('Bearer ')) {
			// Remove Bearer from string
			token = token.slice(7, token.length);
		}

		client.set(token, 'invalidated', function (err, reply) {
			console.log(reply);
			return res.status(200).json({ success: 'The token has been invalidated' });
		});
	},
	getById: function (req, res, next) {
		userModel.findById(req.params.id, function (err, userInfo) {
			if (err) {
				res.status(404).json({ error: 'User not found' })
				next();
			} else {
				res.status(200).json({ data: { user: userInfo } });
			}
		});
	},
}