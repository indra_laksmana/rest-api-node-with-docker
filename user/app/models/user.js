const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;
const UserSchema = new Schema({
	_id:{
		type: String,
		trim: true
	},
	name: {
		type: String,
		trim: true,
		required: true,
	},
	username: {
		type: String,
		trim: true,
		required: true,
	},
	email: {
		type: String,
		trim: true,
		required: true
	},
	password: {
		type: String,
		trim: true,
		required: true
	},
	phone: {
		type: String,
		trim: true
	},
	address: {
		type: String,
		trim: true
	},
	created_at: { 
		type: Date, 
		default: Date.now 
	}
});

module.exports = mongoose.model('User', UserSchema);