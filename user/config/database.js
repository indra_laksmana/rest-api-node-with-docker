//Set up mongoose connection
const mongoose = require('mongoose');
const config = require('./app');

mongoose.connect(config.database, {
	useNewUrlParser: true
}).then(() => {
	console.log('Successfully connected to MongoDB');
}).catch(err => {
	console.log('Could not connect to MongoDB. Exiting now...', err);
	process.exit();
});

mongoose.Promise = global.Promise;
module.exports = mongoose;