const express = require('express');
const router = express.Router();
const multer = require('multer');
const formData = multer();

const userController = require('../app/controllers/user');

router.post('/register', formData.fields([]), userController.register);
router.post('/login', formData.fields([]), userController.login);
router.get('/authenticated', userController.me);
router.get('/invalidate', userController.invalidate);

module.exports = router;