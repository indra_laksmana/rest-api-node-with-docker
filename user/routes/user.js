const express = require('express');
const router = express.Router();

const userController = require('../app/controllers/user');

router.get('/:id', userController.getById);

module.exports = router;